# GCHQ 4.0 (HQ++)

## Raumplan

Ein Raumplan in SVG, ist von den Plänen des Mietvertrags 1 zu 1 abgezeichnet
worden.

Daraus wurde die Basis für ein Blender Modell.

## Elektroinstallation


Für die Erstellung des Elektroinstallationsplanes
wurde die Software [QEletrocTech](https://qelectrotech.org/) verwendet.


